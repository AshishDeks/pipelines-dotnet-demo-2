using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MathDemo
{
    public class MathService
    {
        public int Add(int x, int y)
        {
            return x + y;
        }
        
        public int Subtract(int x, int y)
        {
            return x - y;
        }
    }
}
